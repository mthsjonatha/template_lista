# Modelo em LaTeX para o PIBID

Todos os arquivos não estão em versão final.

Você pode baixar, alterar e distribuir a vontade, desde que referencia o autor e o link do arquivo original.

Link desta pasta é : **https://github.com/mthsjonatha/pibid_lista**

O author: **Matheus Jonatha**

Para contado, manda e-mail para **mthsjonatha@gmail.com**

**OBS :** Vale salientar que não são modelos oficiais e não há qualquer ligação oficial com as instituições responsaveis, não sendo assim de responsabilidade das mesma quaisquer problemas.